package com.example.demo.config;

import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class InitQueueConfig {

  @Bean
  Queue initTestQueue(){
    return QueueBuilder
        .durable("TEST_QUEUE_NAME")
        .build();
  }

  @Bean
  Binding bindTestQueue(Queue initTestQueue, TopicExchange testExChange){
    return BindingBuilder
        .bind(initTestQueue)
        .to(testExChange)
        .with("test.queue.*");
  }

}
