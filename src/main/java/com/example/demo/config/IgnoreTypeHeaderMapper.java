package com.example.demo.config;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.support.converter.DefaultJackson2JavaTypeMapper;

public class IgnoreTypeHeaderMapper extends DefaultJackson2JavaTypeMapper {

  @Override
  public JavaType toJavaType(MessageProperties properties){
    try{
      return super.toJavaType(properties);
    }catch (IllegalArgumentException e){
      return this.hasInferredTypeHeader(properties) ? this.fromInferredTypeHeader(properties)
          : TypeFactory.defaultInstance().constructType(Object.class);
    }
  }

}
