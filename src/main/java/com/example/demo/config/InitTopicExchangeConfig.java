package com.example.demo.config;

import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class InitTopicExchangeConfig {

  @Bean
  public TopicExchange testExChange(){
    return new TopicExchange("test_queue");
  }
}
