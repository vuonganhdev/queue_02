package com.example.demo;

import com.example.demo.config.IgnoreTypeHeaderMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Bean
	Jackson2JsonMessageConverter amqpMessageConverter(ObjectMapper objectMapper){
		final Jackson2JsonMessageConverter jackson2JsonMessageConverter = new Jackson2JsonMessageConverter(objectMapper);
		jackson2JsonMessageConverter.setJavaTypeMapper(new IgnoreTypeHeaderMapper());
		return jackson2JsonMessageConverter;
	}

}
