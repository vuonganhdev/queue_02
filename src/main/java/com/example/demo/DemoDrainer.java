package com.example.demo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class DemoDrainer {

  @RabbitListener(
      queues = "TEST_QUEUE_NAME"
  )
  public void testListener(Message message){
    log.info("Hello ");
  }

}
