1. Add libraries to POM. 
2. init TypeMapper (IgnoreTypeHeaderMapper)
3. init @Bean amqpMessageConverter on Application
4. init @Bean exchange (InitTopicExchangeConfig)
5. init @Bean Queue and Binding (InitQueueConfig)
6. init @Bean RabbitTemplate (RabbitConfig)
7. init @Bean RabbitListener (DemoDrainer)